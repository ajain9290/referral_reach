# Preview all emails at http://localhost:3000/rails/mailers/referal_mailer
class ReferralMailerPreview < ActionMailer::Preview
  def invite_email
    referral = Referral.new(email: "joe@gmail.com")
    user = User.new(email: 'preview@referral_reach.com')

    ReferralMailer.with(referral: referral, user: user).invite_email
  end
end
