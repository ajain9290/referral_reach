# Referral Reach

Referral reach app allows user to sign up or login and then add referrals and send them invitation to join our platform. We are using `devise` and `jwt-devise` for the API authentication. We have inbuilt react app `referrable` that is responsible for rendering the UI. Our app uses `sidekiq`  to send emails in the background and show preview in the `letter opener` gem.


# Prerequisites

- Ruby - 3.0.1
- Rails - 7.0.1
- node - 14.17.0
- MySQL - 0.5
- Redis

## Installation

 1. Clone above repository in your local system.
 2. Make sure you have correct version of ruby installed in your system.
 3. After cloning go to the folder where repository is installed `cd referral_reach`.
 4. Run `bin/setup` to setup the project and make sure there is no error.
 5. Run `bin/dev` to run the app. Your server will run on the port `3000` and frontend on the port `3001`.

