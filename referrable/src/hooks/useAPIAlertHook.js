import { useContext } from 'react';
import { APIAlertContext } from '../providers/APIAlertProvider';

function useAPIAlertHook() {
  const { type, text, showAlert, setAlert } = useContext(APIAlertContext);
  return { type, text, showAlert, setAlert };
}

export default useAPIAlertHook;
