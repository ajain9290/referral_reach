export function parseJSON(jsonStr) {
  try {
    return JSON.parse(jsonStr);
  } catch (e) {
    return { error: jsonStr };
  }
}
