import { json, redirect } from 'react-router-dom';
import { apiURL } from '../constants';
import { parseJSON } from './common';

async function AuthAction({ request }) {
  const searchParams = new URL(request.url).searchParams;
  const mode = searchParams.get('mode') || 'signup';

  if (mode !== 'login' && mode !== 'signup') {
    throw json({ message: 'Unsupported mode.' }, { status: 422 });
  }

  const data = await request.formData();
  const authData = {
    user: {
      email: data.get('email'),
      password: data.get('password'),
    }
  };

  const response = await fetch(apiURL + mode, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(authData),
  });

  if (response.status === 500) {
    throw json({ message: 'Could not authenticate user.' }, { status: 500 });
  } else if (!response.ok) {
    const error = await response.text();
    const parsedError = parseJSON(error);

    return parsedError;
  } else {
    const token = await response.headers.get('Authorization');
    localStorage.setItem('token', token);
  }

  // history.push('/login');
  return redirect('/referrals');
}

export default AuthAction;
