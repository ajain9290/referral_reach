import { json, redirect } from 'react-router-dom';

import { apiVersionURL } from '../constants';
import { getAuthToken } from '../util/auth';
import { parseJSON } from './common';

async function ReferralAction({ request, params }) {
  const method = request.method;
  const data = await request.formData();
  const token = getAuthToken();
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': token
  };

  let url = apiVersionURL + '/referrals';
  let response = {};

  if (!token) {
    throw json({ message: 'User is not logged in' }, { status: 401 });
  }

  if (method === 'POST') {
    const referralData = {
      referral: { email: data.get('email') },
    };

    response = await fetch(url, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(referralData),
    });
  } else {
    const searchParams = new URL(request.url).searchParams;
    const referralId = searchParams.get('referralId');
    url = url + '/' + referralId;

    response = await fetch(url, {
      method: 'PUT',
      headers: headers,
    });
  }

  if (response.status === 500) {
    throw json({ message: 'Could not sent invite.' }, { status: 500 });
  } else if (!response.ok) {
    const error = await response.text();
    const parsedError = parseJSON(error);

    return parsedError;
  }

  return redirect('/referrals');
}

export default ReferralAction;
