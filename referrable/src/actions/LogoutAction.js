import { json, redirect } from "react-router-dom";

import { getAuthToken } from "../util/auth";
import { apiURL } from '../constants';
import { parseJSON } from './common';

async function LogoutAction() {
  const token = getAuthToken();

  const response = await fetch(apiURL + 'logout', {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
  });

  if (response.status === 500) {
    throw json({ message: 'Could not logout user.' }, { status: 500 });
  } else if (!response.ok) {
    const error = await response.text();
    const parsedError = parseJSON(error);

    localStorage.removeItem('token');

    return parsedError;
  } else {
    localStorage.removeItem('token');
  }

  return redirect('/');
}

export default LogoutAction;
