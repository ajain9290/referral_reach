import { useNavigation, Form } from 'react-router-dom';

import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import SendIcon from '@mui/icons-material/Send';

function ReferralForm() {
  const navigation = useNavigation();
  const isSending = navigation.state === 'submitting';

  return (
    <Box component={Form} method='post' validate='true' sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
          />
        </Grid>
        <Grid item xs={2}>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            disabled={isSending}
          >
            { isSending ? 'Sending...' :
              (
                <>
                  Send
                  <SendIcon sx={{ml: 2}} />
                </>
              )
            }
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}

export default ReferralForm;
