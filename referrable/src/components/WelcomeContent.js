import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

function WelcomeContent() {
  return (
    <Container component="main">
      <Box sx={{ flexGrow: 1 }}>
        <Typography component="h1" variant="h5">
            Welcome!
        </Typography>
      </Box>
    </Container>
  )
}

export default WelcomeContent
