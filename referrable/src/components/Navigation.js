import { Link, useRouteLoaderData, Form } from 'react-router-dom';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

import Breadcrumb from './Breadcrumb';

function Navigation() {
  const token = useRouteLoaderData('root');

  const renderLoginButtons = () => {
    if(token) {
      return(
        <Form action='/logout' method='post'>
          <Button color="inherit" type='submit'>Logout</Button>
        </Form>
      );
    } else {
      return(
        <>
          <Button color="inherit" component={Link} to={'/auth?mode=login'}>Login</Button>
          <Button color="inherit" component={Link} to={'/auth'}>Signup</Button>
        </>
      );
    }
  }

  return (
    <Box sx={{ flexGrow: 1, mt: 3 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Breadcrumb token={token} />
          {renderLoginButtons()}
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Navigation;
