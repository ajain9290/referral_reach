import { Link, Form } from 'react-router-dom';

import Button from "@mui/material/Button";
import { DataGrid } from '@mui/x-data-grid';

const renderResendButton = (cellValues) => {
  const { row } = cellValues;

  return (
    <>
      {
        row.can_be_resend && (
          <Form
            action={'/referrals/resend?referralId='+ row.id}
            method='put'
            sx={{ alignItems: 'center'}}
          >
            <Button variant='contained' color='primary' type='submit'>
              Resend
            </Button>
          </Form>
        )
      }
    </>
  );
}
const columns = [
  { field: 'id', hide: true },
  { field: 'email', headerName: 'Referred Emails', width: 400 },
  { field: 'status', headerName: 'Status', width: 200 },
  { field: 'last_sent_at', headerName: 'Last Sent At', width: 200 },
  { field: "Resend", renderCell: renderResendButton, width: 300 }
];

function ReferralList({ referrals }) {
  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={referrals}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 5 },
          },
        }}
        pageSizeOptions={[5, 10]}
        checkboxSelection
      />
    </div>
  );
}

export default ReferralList;
