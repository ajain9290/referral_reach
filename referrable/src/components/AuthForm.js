import { useEffect } from 'react';
import {
  Link as RouterLink,
  useSearchParams,
  useNavigation,
  Form,
  useActionData,
} from 'react-router-dom';

import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";

import useAPIAlertHook from '../hooks/useAPIAlertHook';

function AuthForm() {
  const navigation = useNavigation();
  const [searchParams] = useSearchParams();
  const isLogin = searchParams.get('mode') === 'login';
  const isSubmitting = navigation.state === 'submitting';
  const { setAlert } = useAPIAlertHook();
  const data = useActionData();

  useEffect(() => {
    if (data && data.error) {
      setAlert('error', data.error)
    }
  }, [data, setAlert, navigation]);

  const renderLoginButtons = () => {
    return(
      <>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Log In
        </Button>
        <Grid container>
          <Grid item>
            <Link component={RouterLink} to="/auth"variant="body2">
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
      </>
    );
  }

  const renderSignupButtons = (isSubmitting) => {
    return(
      <Grid container spacing={2} sx={{ justifyContent: "center" }}>
        <Grid item xs={12} sm={12} md={4} lg={4} textAlign={'center'}>
          <Button
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            component={RouterLink}
            to='/?mode=login'
          >
          Cancel
          </Button>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} textAlign={'center'}>
          <Button
            type="submit"
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            disabled={isSubmitting}
          >
            {isSubmitting ? 'Submitting...' : 'Save'}
          </Button>
        </Grid>
      </Grid>
    );
  }

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          {isLogin ? 'Log In' : 'Sign up'}
        </Typography>
        <Box
          component={Form}
          method='post'
          validate='true'
          sx={{ mt: 1 }}
        >
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          {isLogin ? renderLoginButtons() : renderSignupButtons(isSubmitting)}
        </Box>
      </Box>
    </Container>
  );
}

export default AuthForm;
