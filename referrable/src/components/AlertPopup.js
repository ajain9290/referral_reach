import { Alert } from '@mui/material';
import useAPIAlertHook from '../hooks/useAPIAlertHook';

function AlertPopup() {
  const { text, type, showAlert } = useAPIAlertHook();

  if (text && type && showAlert) {
    return (
      <Alert severity={type} sx={{ zIndex: 10 }}>
        {text}
      </Alert>
    );
  } else {
    return null;
  }
};

export default AlertPopup;
