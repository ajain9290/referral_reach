import { NavLink } from 'react-router-dom';

import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';

function Breadcrumb({ token }) {
  return (
    <Breadcrumbs
      separator=""
      aria-label="breadcrumb"
      color="inherit"
      sx={{ flexGrow: 1 }}
    >

      <Typography
        component={NavLink}
        to='/'
        color='inherit'
        underline="hover"
        sx={{ mr: 2, textDecoration: 'none' }}
      >
        Home
      </Typography>

      { token && (
        <Typography
          component={NavLink}
          to='/referrals'
          color='inherit'
          underline="hover"
          sx={{ mr: 2, textDecoration: 'none' }}
        >
          Referrals
        </Typography>
      )}

      { !token && (
        <Typography
          component={NavLink}
          to='/auth?mode=login'
          color='inherit'
          underline="hover"
          sx={{ mr: 2, textDecoration: 'none' }}
        >
          Authentication
        </Typography>
      )}


    </Breadcrumbs>
  )
}

export default Breadcrumb
