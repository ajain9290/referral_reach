import { Suspense } from 'react';
import { useLoaderData, Await } from 'react-router-dom';

import Container from "@mui/material/Container";
import LinearProgress from '@mui/material/LinearProgress';

import ReferralForm from "../components/ReferralForm";
import ReferralList from "../components/ReferralList";

export default function ReferralsPage() {
  const { referrals } = useLoaderData();

  return (
    <Container component="main">
      <ReferralForm />
      <Suspense fallback={<LinearProgress />}>
        <Await resolve={referrals}>
          { (loadedRefferals) => <ReferralList referrals={loadedRefferals} /> }
        </Await>
      </Suspense>
    </Container>
  );
};


