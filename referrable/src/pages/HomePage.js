import { useRouteLoaderData } from 'react-router-dom';

import Container from "@mui/material/Container";
import { Typography } from '@mui/material';

const HomePage = () => {
  const token = useRouteLoaderData('root');
  const text = token ? 'Navigate to referrals page.' : 'Please login to see your referrals';

  return(
    <Container
      sx={{
        mt: 3,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        minHeight: '100vh',
        minWidth: '100%',
        backgroundColor: 'primary.light',
      }}
    >
      <div>
        <Typography variant='h2'>
            Welcome to Referral Reach!
        </Typography>
        <Typography variant='h5' sx={{ mt: 2 }}>
          {text}
        </Typography>
      </div>
    </Container>
  );
}

export default HomePage;
