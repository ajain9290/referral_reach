import { useRouteError } from 'react-router-dom';

import Container from "@mui/material/Container";
import { Typography } from '@mui/material';
import { grey } from '@mui/material/colors';

import Breadcrumb from '../components/Navigation';

function ErrorPage() {
  const error = useRouteError();

  console.log(error);

  let status = 'Bad Request';
  let message = 'Something went wrong!';

  if (error.status === 500) {
    status = 'Internal Server Error';
    message = error.data.message;
  }

  if (error.status === 404) {
    status = 'Not found!';
    message = 'Could not find resource or page.';
  }

  return (
    <>
      <Breadcrumb />
      <Container
        sx={{
          mt: 3,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        color: 'white',
        minHeight: '100vh',
        minWidth: '100%',
        backgroundColor: grey[500],
          '&:hover': {
            opacity: [0.9, 0.8, 0.7],
          },
        }}
      >
        <div>
          <Typography variant='h2'>
            { status }
          </Typography>
          <Typography variant='h3'>
            { message }
          </Typography>
        </div>
      </Container>
    </>
  )
}

export default ErrorPage;
