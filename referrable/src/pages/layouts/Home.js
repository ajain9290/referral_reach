import { Outlet } from 'react-router-dom';

import Breadcrumb from '../../components/Navigation';
import AlertPopup
 from '../../components/AlertPopup';
function HomeLayout() {
  return (
    <>
      <Breadcrumb />
      <AlertPopup showAlert={false} />
      <Outlet />
    </>
  );
}

export default HomeLayout;
