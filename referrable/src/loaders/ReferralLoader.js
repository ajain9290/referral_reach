import { json, defer } from 'react-router-dom';

import { apiVersionURL } from '../constants';
import { getAuthToken } from '../util/auth';

async function loadReferrals() {
  const response = await fetch(apiVersionURL + '/referrals', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': getAuthToken(),
    },
  });

  if (response.status === 500) {
    throw json({ message: 'Could not fetch referrals.' }, { status: 500 });
  }
  else if (!response.ok) {
    return response;
  } else {
    const resData = await response.json();
    return resData;
  }
}

export default function ReferralLoader() {
  return defer({
    referrals: loadReferrals(),
  });
}


