import { RouterProvider, createBrowserRouter } from 'react-router-dom';

import Container from "@material-ui/core/Container";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { purple } from '@mui/material/colors';

import HomePage from './pages/HomePage';
import ReferralsPage from './pages/ReferralsPage';
import AuthenticationPage from './pages/AuthenticationPage';
import HomeLayout from './pages/layouts/Home';
import AuthAction from './actions/AuthAction';
import ErrorPage from './pages/ErrorPage';
import LogoutAction from './actions/LogoutAction';
import ReferralAction from './actions/ReferralAction';
import ReferralLoader from './loaders/ReferralLoader';
import APIAlertProvider from './providers/APIAlertProvider';
import { tokenLoader } from './util/auth';
import { checkAuthLoader } from './util/auth';

import './App.css';

const router = createBrowserRouter([
  {
    path: '/',
    element: <HomeLayout />,
    errorElement: <ErrorPage />,
    id: 'root',
    loader: tokenLoader,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/auth', element: <AuthenticationPage />, action: AuthAction },
      {
        path: '/referrals',
        loader: checkAuthLoader,
        children: [
          {
            index: true,
            element: <ReferralsPage />,
            loader: ReferralLoader,
            action: ReferralAction,
          },
          { path: 'resend', action: ReferralAction }
        ]
      },
    ]
  },
  { path: '/logout', action: LogoutAction, errorElement: <ErrorPage /> }
])

const theme = createTheme({
  palette: {
    primary: purple,
  }
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <APIAlertProvider>
        <Container>
          <RouterProvider router={router} />
        </Container>
      </APIAlertProvider>
    </ThemeProvider>
  );
}

export default App;
