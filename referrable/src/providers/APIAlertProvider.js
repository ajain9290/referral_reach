import { useState, createContext } from 'react'

export const APIAlertContext = createContext({
  text: null,
  type: null,
  showAlert: false,
  setAlert: () => {},
});

function APIAlertProvider({ children }) {
  const [text, setText] = useState(null);
  const [type, setType] = useState(null);
  const [showAlert, setShowAlert] = useState(false);

  const setAlert = (type, text) => {
    if (!showAlert) {
      setType(type);
      setText(text);
      setShowAlert(true);

      setTimeout(() => {
        setType(null);
        setText(null);
        setShowAlert(false);
      }, 5000);
    }
  };


  return (
    <APIAlertContext.Provider
      value={{
        text,
        type,
        setAlert,
        showAlert,
      }}
    >
      {children}
    </APIAlertContext.Provider>
  )
}

export default APIAlertProvider
