class ReferralEmailsJob
  include Sidekiq::Job

  def perform(referral_id, user_id)
    referral = Referral.find(referral_id)
    user = User.find(user_id)

    ReferralMailer.with(referral: referral, user: user).invite_email.deliver
  end
end
