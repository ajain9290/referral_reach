class Referral < ApplicationRecord
  belongs_to :user

  enum :status, %i(invited signed_up)

  validates :email, presence: true, uniqueness: { case_sensitive: false, allow_blank: true }
end
