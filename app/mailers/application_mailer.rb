class ApplicationMailer < ActionMailer::Base
  default from: "team@referral_reach.com"
  layout "mailer"
end
