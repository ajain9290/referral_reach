class ReferralMailer < ApplicationMailer
  def invite_email
    @referral = params[:referral]
    @user = params[:user]

    mail(to: @referral.email, subject: "You have been invited to Referral Reach")
  end
end
