# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  include RackSessionsFix

  respond_to :json

  private

  def respond_with(user, _opts = {})
    resource.persisted? ? respond_with_success(user) : respond_with_failure(user)
  end

  def respond_with_success(user)
    referral.signed_up! if referral.present?

    render json: {
      message: 'Signed up successfully.' ,
      data: { email: user.email }
    }, status: :ok
  end

  def respond_with_failure(user)
    render json: {
      error: user.errors.full_messages.to_sentence
    }, status: :unprocessable_entity
  end

  def referral
    @referral ||= Referral.find_by(email: current_user.email)
  end
end
