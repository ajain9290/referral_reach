# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  include RackSessionsFix

  respond_to :json

  private

  def respond_with(current_user, _opts = {})
    render json: {
      message: 'Logged in successfully.',
      data: { user: current_user.email }
    }, status: :ok
  end

  def respond_to_on_destroy
    current_user? ? log_out_success : log_out_failure
  rescue JWT::ExpiredSignature
    log_out_failure
  end

  def log_out_success
    render json: { message: "Logged out successfully." }, status: :ok
  end

  def log_out_failure
    render json: { error: "Couldn't find an active session."}, status: :unauthorized
  end

  def current_user?
    session_token = request.headers['Authorization']&.split(' ')&.last
    return if session_token.nil?

    jwt_payload = JWT.decode(
      session_token,
      Rails.application.credentials.devise_jwt_secret_key!
    ).first

    User.find(jwt_payload['sub']).present?
  end
end
