class Api::V1::ReferralsController < ApplicationController
  before_action :authenticate_user!

  def index
    referrals = current_user.referrals.order(updated_at: :desc)
    render json: referrals, each_serializer: ::ReferralSerializer
  end

  def create
    referral = current_user.referrals.new(referral_params)
    referral.last_sent_at = Time.now

    if User.find_by(email: referral_params[:email]).present?
      render json: {
        error: "User with email #{referral_params[:email]} has already signed up."
      }, status: :unprocessable_entity
    elsif referral.save
      ReferralEmailsJob.perform_async(referral.id, current_user.id)

      render json: { message: "Your referral has been sent successfully" }, status: :ok
    else
      render json: { error: referral.errors.full_messages.to_sentence }, status: :unprocessable_entity
    end
  end

  def update
    referral = Referral.find_by(id: params[:id])

    if referral.blank?
      render json: { error: "Referral not found with id #{params[:id]}" }, status: :not_found
    elsif referral.update(last_sent_at: Time.now)
      ReferralEmailsJob.perform_async(referral.id, current_user.id)

      render json: { message: "Your referral has been re-sent successfully" }, status: :ok
    else
      render json: { error: referral.errors.full_messages.to_sentence }, status: :unprocessable_entity
    end
  end

  private

  def referral_params
    params.require(:referral).permit(:email)
  end
end
