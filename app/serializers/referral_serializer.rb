class ReferralSerializer < ActiveModel::Serializer
  attributes :id, :email, :status, :last_sent_at, :can_be_resend

  def status
    object.invited? ? 'Invited' : 'Signed Up'
  end

  def can_be_resend
    object.invited? ? true : false
  end

  def last_sent_at
    object.last_sent_at&.strftime("%-d/%-m/%y: %H:%M %Z")
  end
end
