class CreateReferrals < ActiveRecord::Migration[7.0]
  def change
    create_table :referrals do |t|
      t.string :email, null: false
      t.integer :status, default: 0
      t.datetime :last_sent_at
      t.references :user, foreign_key: true, index: true

      t.timestamps
    end

    add_index :referrals, :email, unique: true
  end
end
